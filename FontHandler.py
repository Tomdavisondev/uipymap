from PIL import Image,ImageDraw,ImageFont
from fontTools.ttLib import TTFont


class FontHandler:

    def __init__(self):
        # TODO: Allow the user to enter a font file as a parameter
        print("Saving Images from supplied font file")
        print("=================================================")
        self.saveimages()

    def saveimages(self):

        fontIter = TTFont('arial.ttf')
        ch_to_name = {}  # key will be the codepoint in hex, value will be name

        cmap = fontIter["cmap"]
        for ch, name in cmap.getBestCmap().items():
            ch_to_name["{:04X}".format(ch)] = name
            if len(name) < 2:
                self.generateimage(name)

        print("Images Saved!")

    def generateimage(self, name):
        font = ImageFont.truetype("arial.ttf", 28, encoding="unic")
        # get the line size
        text_width, text_height = font.getsize(name)

        # create a blank canvas with extra space between lines
        canvas = Image.new('RGB', (text_width + 10, text_height + 10), "white")

        # draw the text onto the text canvas, and use black as the text color
        draw = ImageDraw.Draw(canvas)
        draw.text((5, 5), name, 'black', font)

        # save the blank canvas to a file
        canvas.save("FontData/" + name + ".png", "PNG")