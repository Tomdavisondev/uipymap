# PyWordFind Prototype

A simple and lightweight utility to "read" text from images to a .CSV file

### Architecture

The prototype is split into 3 modules with the aim being that each individual module can be split and plugged into other projects easily

#### Module 1 - InterfaceGetter

Will grab and process photos of the desired text from the UI source. In this case multiple products from a product page

#### Module 2 - OCR

The image will be processed and piped into TesseractOCR. Tesseract will then generate text. If the InterfaceGetter is robust enough and the sample is relatively simple.

#### Module 3 - Spellcheck

Module 3 flags text returned from TesseractOCR based on grammar and spelling. If incorrect then the image will be flagged

```
Implementation regardling flagging is currently undecided. The image could be piped back into the OCR and by flagging different pre-processing techniques could be applied until we find the "best" result
```

```
The alternative would be flagging for human validation
```

#### Module 4 - CSV Writer

The CSV writer will grab the information returned by Tesseract and put the data into a spreadsheet, listing the following:

* Product
* Price
* A list of characteristics related to the product