# USAGE
# python ocr.py --image images/example_01.png
# python ocr.py --image images/example_02.png  --preprocess blur

# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os

class UIChecker:

    def __init__(self):
        self.ConvertImage()

    def ConvertImage(self):
        pytesseract.pytesseract.tesseract_cmd = 'C:\Program Files\Tesseract-OCR\/tesseract.exe'

        image = cv2.imread('SampleData/ImageTest.png')

        cv2.imshow("Image", image)

        # write the scale image to disk as a temporary file so we can
        # apply OCR to it
        filename = "{}.png".format(os.getpid())
        cv2.imwrite(filename, image)

        # load the image as a PIL/Pillow image, apply OCR, and then delete
        # the temporary file
        text = pytesseract.image_to_string(Image.open(filename))
        os.remove(filename)
        print(text)

        # show the output images
        cv2.imshow("Image", image)
        cv2.imshow("Output", image)
        cv2.waitKey(0)